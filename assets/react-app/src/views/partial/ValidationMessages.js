import React from 'react';

const ValidationMessages = ({ errors }) => {
    let messageArray = [];
    for(let prop in errors){
        messageArray.push({field: prop, message : errors[prop]});
    }
    if (messageArray.length) {
        return (
            <div className={'alert alert-danger'}>
                {messageArray.map((item) => (<div>{item.field} : {item.message}</div>))}
            </div>
        );
    }

    return null;
};

export default ValidationMessages;