import React, { Component } from 'react';
import Loading from './partial/Loading'
import Error from './partial/Error'
import ValidationMessages from './partial/ValidationMessages'

export default class CreateContactView extends Component {
    formValues = {};

    onInputChange = (event) => {
        this.formValues[event.target.name] = event.target.value;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.createContact(this.formValues);
    }

    render() {
        const { validationErrors, successMessage, failureMessage, loading } = this.props.contact;
        return (
            <>
                {!successMessage &&
                <form onSubmit={this.handleSubmit} autoComplete={'off'}>
                    <div className="form-group">
                        <input
                            type="text"
                            className={'form-control'}
                            placeholder="Email"
                            name="email"
                            onChange={this.onInputChange}
                        />
                    </div>
                    <div className="form-group">
                            <textarea
                                className={'form-control'}
                                placeholder="Message"
                                name="message"
                                onChange={this.onInputChange}
                            />
                    </div>
                    <div className={'form-group'}>
                        <input type="submit" value="Send" className={'btn btn-sm btn-info'}/>
                    </div>
                    <Error message={failureMessage}/>
                </form>
                }

                <Loading loading={loading}/>
                <ValidationMessages errors={validationErrors}/>
                {successMessage ? successMessage : ''}
            </>
        );
    }
}
