import React, { Component } from 'react';
import CreateContactContainer from '../containers/CreateContactContainer';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import '../style/App.css';

class AppView extends Component {
    render() {
        return (
            <div className="container ">
                <Router>
                    <div>
                        <Route exact path="/" component={CreateContactContainer}/>
                    </div>
                </Router>
            </div>
        );
    }
}

export default AppView;
