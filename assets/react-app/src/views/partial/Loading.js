import React from 'react';

const Loading = ({ loading }) => {
    if (loading) {
        return (
            <div className={'loading'}>Working...</div>
        );
    }

    return null;
};

export default Loading;