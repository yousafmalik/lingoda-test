<?php

namespace App\Controller;

use App\Exception\ValidationException;
use App\Service\ContactService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidatorException;

final class ContactController
{
    use ErrorMessageTrait;
    private $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * @Route("ajax/contact", methods={"POST"})
     */
    public function store(Request $request)
    {
        $contact = $this->contactService->createContactFromRequest($request);

        $errors = $this->contactService->validate($contact);
        if(count($errors)){
            $errorMessages =  $this->getErrorMessages($errors);
            throw new ValidationException(json_encode($errorMessages));
        }

        $contact = $this->contactService->save($contact);

        return new Response(json_encode($contact));
    }
}