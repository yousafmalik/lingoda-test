import { combineReducers } from 'redux';
import createContactReducer from './CreateContactReducer';

const rootReducer = combineReducers({
    contact: createContactReducer,
});

export default rootReducer;
