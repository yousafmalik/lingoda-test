<?php

namespace App\Exception;


use Facebook\WebDriver\Remote\RemoteWebDriver;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

class ValidationException extends \Exception implements HttpExceptionInterface
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode()
    {
        return Response::HTTP_UNPROCESSABLE_ENTITY;
    }

    public function getHeaders()
    {
        return [];
    }
}