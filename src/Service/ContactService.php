<?php

namespace App\Service;

use App\Entity\Contact;
use App\Repository\ContactRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ContactService
{
    protected $validator;
    protected $contactRepository;

    public function __construct(
        ValidatorInterface $validator,
        ContactRepository $contactRepository
    )
    {
        $this->validator = $validator;
        $this->contactRepository = $contactRepository;
    }

    /**
     * Validate contact
     * @param Request $request
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validate(Contact $contact): ConstraintViolationListInterface
    {
        return $this->validator->validate($contact);
    }

    /**
     * @param Contact $contact
     * @return Contact
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Contact $contact): Contact
    {
        $this->contactRepository->save($contact);

        return $contact;
    }

    /**
     * @param Request $request
     * @return Contact
     */
    public function createContactFromRequest(Request $request): Contact
    {
        $contact = new Contact();
        $contact->setEmail($request->get('email', ''));
        $contact->setMessage($request->get('message', ''));

        return $contact;
    }
}