import axios from "axios";

export const CREATE_CONTACT_START = 'CREATE_CONTACT_START';
export const CREATE_CONTACT_SUCCESS = 'CREATE_CONTACT_SUCCESS';
export const CREATE_CONTACT_FAILURE = 'CREATE_CONTACT_FAILURE';

export function createContact(data) {
    return function (dispatch) {
        dispatch({ type: CREATE_CONTACT_START });

        const formData = Object.keys(data).reduce((formData, key) => {
            formData.append(key, data[key]);
            return formData;
        }, new FormData());

        const request = axios.post(
            '/ajax/contact',
            formData,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        );
        const payload = {};

        return request.then((res) => {
            payload.successMessage = 'Successfully submitted';
            dispatch({ payload, type: CREATE_CONTACT_SUCCESS });
        }).catch((error) => {
            if (error.response.status === 422) {
                payload.validationErrors = error.response.data;
            } else {
                payload.failureMessage = 'Ooops, something went wrong';
            }
            dispatch({ payload, type: CREATE_CONTACT_FAILURE });
        });
    }
}