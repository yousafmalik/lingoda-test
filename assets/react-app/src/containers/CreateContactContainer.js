import { connect } from 'react-redux';
import View from '../views/CreateContactView';
import { createContact } from '../actions/ContactAction';

const mapStateToProps = (state) => {
    return {
        contact: state.contact,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        createContact: (formValues) => dispatch(createContact(formValues))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(View);