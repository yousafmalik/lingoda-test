import {
    CREATE_CONTACT_START, CREATE_CONTACT_SUCCESS, CREATE_CONTACT_FAILURE,
} from '../actions/ContactAction';

const initialState = {
    validationErrors: {},
    successMessage: '',
    failureMessage: '',
    loading: false,
};

export default function createContactReducer(state = initialState, action) {
    const payload = action.payload;
    switch (action.type) {
        case CREATE_CONTACT_START: {
            return { ...initialState, loading: true };
        }
        case CREATE_CONTACT_SUCCESS: {
            return { ...initialState, ...payload }
        }
        case CREATE_CONTACT_FAILURE: {
            return { ...initialState, ...payload }
        }
        default:
            return state;
    }
}