<?php

namespace App\Controller;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait ErrorMessageTrait
{
    protected function getErrorMessages(ConstraintViolationListInterface $errors)
    {
        $result = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error){
            $result[$error->getPropertyPath()] = $error->getMessage();
        }

        return $result;
    }
}